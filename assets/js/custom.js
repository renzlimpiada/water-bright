$(document).ready(function(){
  $("#viewall").click(function () {
    $("#category").show();
    $("#slider").hide();
  });
  $("#viewall").click(function () {
    $("#category").show();
    $("#slider").hide();
  });

 $('.multiple-items').slick({
    infinite: true,
    slidesToShow: 5,
    slidesToScroll: 1,
    // autoplay: true,
    autoplaySpeed: 3000,
    arrows: true,
    responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 640,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,

        }
      },
      {
        breakpoint: 426,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: false

        }
      },
      {
        breakpoint: 375,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: false
        }
      }

    ]
    });
    $('.review').slick({
       infinite: true,
       slidesToShow: 3,
       slidesToScroll: 1,
       // autoplay: true,
       autoplaySpeed: 4000,
       arrows: true,
       responsive: [
         {
           breakpoint: 1025,
           settings: {
             slidesToShow: 3,
             slidesToScroll: 1
           }
         },
         {
           breakpoint: 992,
           settings: {
             slidesToShow: 2,
             slidesToScroll: 1
           }
         },
         {
           breakpoint: 641,
           settings: {
             slidesToShow: 1,
             slidesToScroll: 1,
             arrows: false

           }
         }

       ]
    });
    $('#termsandconditionmodal').modal({
        backdrop: 'static',
        keyboard: false
    })
});
